import os

from fabric.api import local, task
from fabric.decorators import runs_once

#BASE_DIR = os.path.sep.join((os.path.dirname(__file__), ''))

@task
@runs_once
def runserver():
    """
    Run the local django server
    """
    local('python manage.py runserver')


@task
@runs_once
def shell():
    """
    Open a django shell
    """
    local('python manage.py shell')


@task
@runs_once
def migrate():
    """
    Apply the necessary migrations
    """
    local('python manage.py migrate')

