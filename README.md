## Task:

Create a REST api to be consumed by a mobile app, which is somewhat similar to TrueCaller (basically a global contact book).

### Data to be stored for each person:
Name, Email, Phone, Current City, Current Company

### Basic Requirements:

* User has to register on the app with their name and phone number before using it.
* User needs to be logged in to access any information.
* User should be able to update his information from the app.
* User can search for a person by name. Search results display the name and phone number for each result matching that name. Clicking a result displays all the details for that person.
* A person's email is only displayed to the user if that person is in the user's contact list.

### Bonus Requirements (high recommended if you have time remaining):

* User can search for a person by phone number. For a person who has an account on the app, the name shown to the user is the one the person provides on his account. Otherwise, show as result only the person who is most likely to have this number. Note that there can be multiple different names for a number since two contact books may have different names for the same phone number.
* User should be able to mark a number as SPAM so that others can use that information to decide whether to pick up an unknown number or not.
* When user receives a call from a number, he should know how likely it is that the number is a SPAM caller (low, medium, high likelihood). You just need some API call that takes a number returns this SPAM likelihood.

### What you can assume:

* The user's contacts are automatically imported from the phone contact book, you don't have to worry about importing it. You just have to store in your app which people are contacts.
* You can use whichever language/framework you're comfortable in.

### To keep in mind:

* Millions of people are using the app simultaneously to look up unknown phone numbers and to search for people's contact details.

### What we care about:

* Think like you're working on production level code and not just an interview "to get it running".
* Don't worry too much about comments, indentation, etc. We care about the code design and algorithms more.
