from contacts.models import Contact

class ContactAuth(object):

    def authenticate(self, phone=None, password=None):
        try:
            user = Contact.objects.get(phone=phone)
            if user.check_password(password):
                return user
            else:
                return None
        except Contact.DoesNotExist:
            return None

    def get_user(self, user_id):
        try:
            user = Contact.objects.get(pk=user_id)
            if user.is_active:
                return user
            return None
        except Contact.DoesNotExist:
            return None
