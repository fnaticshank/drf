from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token

from contacts import views

urlpatterns = [
    url(r'^$', views.api),
    url(r'^auth/token/?$', obtain_jwt_token, name='auth-token'),
    url(r'^contacts/?$', views.contact_list, name="contact-list"),
    url(r'^contacts/(?P<pk>[0-9]+)$', views.contact_detail, name="contact-detail"),
    url(r'^search/?$', views.search, name="search"),
    url(r'^spam/?$', views.check_spam, name="check-spam"),
    url(r'^spam/mark/?$', views.mark_as_spam, name="mark-spam"),
]
