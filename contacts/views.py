import datetime
import jwt

from rest_framework import status
from rest_framework import generics
from rest_framework.reverse import reverse
from rest_framework.response import Response
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated, AllowAny, IsAuthenticatedOrReadOnly
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from django.contrib.auth import authenticate, get_user_model
from contacts.models import Contact
from contacts.serializers import ContactSerializer


@api_view(['GET'])
@authentication_classes((JSONWebTokenAuthentication,))
@permission_classes((AllowAny,))
def api(request):
    return Response({
        'auth-token': reverse('auth-token', request=request),
        'check_spam': reverse('check-spam', request=request),
        'mark_as_spam': reverse('mark-spam', request=request),
        'contacts': reverse('contact-list', request=request),
        'search': reverse('search', request=request),
    })


@api_view(['GET'])
@authentication_classes((JSONWebTokenAuthentication,))
@permission_classes((AllowAny,))
def search(request):
    """
    Search method that searches for contacts via both phone number as well as
    name.
    """
    try:
        if 'name' in request.query_params:
            name = request.query_params['name']
            queryset = Contact.objects.filter(name=name)

            serializer = ContactSerializer(queryset, many=True)
            return Response(serializer.data,status=status.HTTP_200_OK)

        if 'phone' in request.query_params:
            phone = request.query_params['phone']
            queryset = Contact.objects.filter(phone=phone)

            serializer = ContactSerializer(queryset, many=True)
            return Response(serializer.data,status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_400_BAD_REQUEST)

    except Contact.DoesNotExist:
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'POST'])
@authentication_classes((JSONWebTokenAuthentication,))
@permission_classes((IsAuthenticated,))
def contact_list(request, pk):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        try:
            queryset = Contact.objects.filter(pk=pk)
            serializer = ContactSerializer(queryset, many=True)
            return Response(serializer.data, status=status.HTTP_200_OK)
        except Contact.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)

    elif request.method == 'POST':
        serializer = ContactSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT', 'DELETE'])
@authentication_classes((JSONWebTokenAuthentication,))
@permission_classes((IsAuthenticated,))
def contact_detail(request, pk):
    """
    Retrieve, update or delete a contact.
    """
    try:
        contact = Contact.objects.get(pk=pk)
    except Contact.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = ContactSerializer(contact)
        return Response(serializer.data)

    elif request.method == 'PUT':
        serializer = ContactSerializer(contact, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_200_OK)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        contact.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)


@api_view(['PUT'])
@authentication_classes((JSONWebTokenAuthentication,))
@permission_classes((IsAuthenticated,))
def mark_as_spam(request, phone):
    """
    Mark a number as spam
    """
    try:
        contact = Contact.objects.get(phone=phone)
    except Contact.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'PUT':
        contact.mark_as_spam()

        serializer = ContactSerializer(contact)
        serializer.save()
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@authentication_classes((JSONWebTokenAuthentication,))
@permission_classes((AllowAny,))
def check_spam(request):
    """
    Returns the spam level of a number
    """
    try:
        phone = request.query_params['phone']
        contact = Contact.objects.get(phone=phone)
    except Contact.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        spam_status = contact.spam_status()
        return Response({'spam_status:', spam_status}, status=status.HTTP_200_OK)

