from time import timezone

from django.conf import settings
from django.contrib.auth.models import BaseUserManager, AbstractBaseUser, AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _


class ContactManager(BaseUserManager):

    def _create_user(self, name, phone, password, is_staff, is_superuser, **extra_fields):
        """Creates and saves a User with the given phone number and password"""
        now = timezone.now()

        if not phone:
            raise ValueError("Phone number can't be empty")

        user = self.model(phone=phone, name=name, is_active=True,
                          is_superuser=is_superuser, is_staff=is_staff,
                          last_login=now, date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, name, phone, password, **extra_fields):
        return self._create_user(name, phone, password, False, False, **extra_fields)

    def create_superuser(self, name, phone, password, **extra_fields):
        return self._create_user(name, phone, password, True, True, **extra_fields)


class Contact(AbstractUser):

    name = models.CharField(max_length=256, default='')
    phone = models.CharField(max_length=32, unique=True, db_index=True )
    city = models.CharField(max_length=100, default='')
    company = models.CharField(max_length=100, default='')
    contacts = models.ManyToManyField('self', symmetrical=False, blank = True)
    spam_meter = models.IntegerField(default=0, editable=False)

    USERNAME_FIELD = 'phone'
    REQUIRED_FIELDS = ['name']

    #objects = ContactManager()

    class Meta:
        verbose_name = 'user'
        verbose_name_plural = 'users'

    def __unicode__(self):
        return self.phone

    def get_full_name(self):
        return self.phone

    def get_short_name(self):
        return self.phone

    def mark_as_spam(self):
        self.spam_meter += 1

    def spam_status(self):
        """Returns the likelihood of a contact being a spam: low, medium or high"""
        if self.spam_meter < 40:
            return "Low"
        elif self.spam_meter < 100:
            return "Medium"
        else:
            return "High"
